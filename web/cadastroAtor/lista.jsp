<%-- 
    Document   : lista
    Created on : 11/10/2017, 21:17:24
    Author     : Administrador
--%>

<%@page import="java.util.List"%>
<%@page import="br.com.senac.modelo.Ator"%>
<%@page import="br.com.senac.banco.AtorDAO"%>
<jsp:include page="../header.jsp" />

<%
    AtorDAO dao = new AtorDAO();
    List<Ator> lista = dao.listarTodos();
%>

<div class="container">
    <fieldset>
        <legend>Lista de Atores</legend> 

        <table class="table table-hover">
            <thead>
                <tr>
                    <td>C�digo</td><td>Primeiro Nome </td><td>Ultimo Nome</td>
                </tr>
            </thead>
            <tbody>
                <% for (Ator ator : lista) {%>
                <tr>
                    <td><%= ator.getCodigo()%></td>
                    <td><%= ator.getPrimeiroNome()%></td>
                    <td><%= ator.getUltimoNome()%> </td>
                </tr>

                <%}%>
            </tbody>
        </table>
    </fieldset>
</div>

<jsp:include page="../footer.jsp" />